local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")

-- ################################################################################
-- SET VARIABLES
local packagename="vlc-daemon"
local processname="vlc"
local configfile = "/etc/conf.d/"..tostring(processname)
local logfile = "/var/log/vlc/vlc.log"

function mymodule.set_processname(p)
	processname = p
	configfile = "/etc/conf.d/"..tostring(processname)
end


-- ################################################################################
-- LOCAL FUNCTIONS


-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, tostring(processname).." Status")
end

function mymodule.getconfigfile()
	-- FIXME Validate
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.get_logfile(self, clientdata)
	local retval = cfe({ type="group", value={}, label="Log File Configuration" })
	retval.value.filename = cfe({value=logfile, label="File name"})
	local path
	if fs.is_file(configfile) then
		for line in string.gmatch(fs.read_file(configfile) or "", "(.-)\n") do
			path=string.match(line,"^VLC_OPTS.*--logfile%s+(%S+)") or path
		end
	end
	if path then retval.value.filename.value = path end
	return retval
end

function mymodule.setconfigfile(self, filedetails)
	-- FIXME Validate
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.getleases()
	return modelfunctions.getfiledetails(leasefile)
end

return mymodule
