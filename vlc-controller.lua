local mymodule = {}

mymodule.mvc = {}
mymodule.mvc.on_load = function(self, parent)
	self.model.set_processname(string.match(self.conf.prefix, "[^/]+"))
end

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.getconfigfile, self.model.setconfigfile, self.clientdata, "Save", "Edit Config File", "Configuration File Set")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
